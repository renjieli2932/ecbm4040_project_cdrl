# ECBM 4040 PROJECT
# Team: CDRL
# Renjie Li, Chenghao Diao

# Modification for Jupyter notebook

import tensorflow as tf
import numpy as np
import utils
import os
import time

    
def main(content_layers=['conv1_1'],style_layers=['conv1_1']):
    #############################################
    ##########  General Parameters     ##########
    #############################################

    global_width = 400
    global_height = 300

    model_path = './imagenet-vgg-verydeep-19.mat' # Pre-trained model

    #############################################
    ##########  Image Preprocessing    ##########
    #############################################
    ImageIO = utils.ImageIO()
    content_path = './Images/Neckarfront.jpg' # customizable
    style_path = './Images/StarryNight.jpg' # customizable
    content_img = ImageIO.read(content_path)
    style_img = ImageIO.read(style_path)

    # Random White Noise Image
    gen_img = np.random.normal(loc=0,scale=10,size=(1,global_height,global_width,3))



    #############################################
    ##########  Custom Parameters      ##########
    #############################################


    #content_layers = ['conv1_1'] #customizable
    content_weight = 1.0 / len(content_layers)
    #style_layers = ['conv1_1','conv2_1','conv3_1','conv4_1','conv5_1'] #customizable
    style_weight = 1.0 / len(style_layers)

    num_of_img = 5 # how many images you want to save during iteration


    print('Welcome to Group CDRL\'s Project! \nPlease Input the Reconstruction Info ! ')
    iteration = int(input('Input the number of iterations: \n'))
    learning_rate = float(input('Input the learning rate for Adam Optimizer:\n'))
    reconstruction_mode = int(input('Input the mode:\n 1. Style Reconstruction\n 2. Content Reconstruction\n 3. Combination of Both\n'))
    if reconstruction_mode == 3:
        sty_con_ratio = float(input('Input the ratio of style to content: \n'))
    else:
        sty_con_ratio = 1


    #############################################
    ##########  Tensorflow             ##########
    #############################################


    with tf.Session() as sess:

        sess.run(tf.global_variables_initializer())
        
        run_time = time.time() # Start Time 

        VGG19 = utils.VGG19(model_path) # Pre-trained model

        # Define total content loss in tensorflow
        sess.run(VGG19['input'].assign(content_img))
        tot_content_loss = 0
        for layer in content_layers:
            #print(layer)
            tot_content_loss += content_weight * utils.content_loss(sess.run(VGG19[layer].output()), VGG19[layer].output())

        # Define total style loss in tensorflow
        sess.run(VGG19['input'].assign(style_img))
        tot_style_loss = 0
        for layer in style_layers:
            #print(layer)
            tot_style_loss += style_weight * utils.style_loss(sess.run(VGG19[layer].output()), VGG19[layer].output())

        # Total Loss
        total_loss =  tot_content_loss +   sty_con_ratio * tot_style_loss

        # Optimizer

        if reconstruction_mode == 1:
            optimizee = tot_style_loss # Style Reconstruction
            folder_name = 'Sty_Adam' + str(learning_rate)
            filename = 'Sty_' + str(len(style_layers)) + '.jpg'
        elif reconstruction_mode == 2:
            optimizee = tot_content_loss # Content Reconstruction
            folder_name = 'Con_Adam' + str(learning_rate)
            filename = 'Con_' + content_layers[0] + '.jpg'
        elif reconstruction_mode == 3:
            optimizee = total_loss # Combination of both
            folder_name = 'Total_Ratio' + str(int(sty_con_ratio)) + '_Adam' + str(learning_rate)
            #filename = 'Total_Ratio' + str(int(sty_con_ratio)) + '.jpg' 
            filename = 'Sty_' + str(len(style_layers)) + '_' + str(int(sty_con_ratio)) + '.jpg'
            #filename = 'swan_rotate_com.jpg'
        #os.mkdir(folder_name)
        train = tf.train.AdamOptimizer(learning_rate).minimize(optimizee)


        # Training

        sess.run(tf.global_variables_initializer())
        sess.run(VGG19['input'].assign(gen_img)) # Random img input
        saver = tf.train.Saver()
        plot_iter = []
        plot_loss = []
        for iter in range(iteration):
            sess.run(train)
            if (iter + 1) % 100 == 0: # save the loss every 100 iterations
                plot_iter.append(iter+1)
                plot_loss.append(sess.run(optimizee))
                if (iter + 1) % (iteration//num_of_img) == 0: 
                    # so there will be only 'num_of_img' times information output to screen
                    print('Iteration:', iter + 1)
                    print('Loss:', sess.run(optimizee))
                if (iter + 1) == iteration: # only save the final images 
                    output = sess.run(VGG19['input'])
                    output_img = ImageIO.write(filename,output)
        
        run_time = time.time() - run_time
        unit_run_time = run_time / iteration * 1000
        #saver.save(sess,"./training_model.ckpt")
        print('Training Time:', run_time) # output the training time
        print('Training Time per 1000 Iterations:', unit_run_time)
        return output_img, plot_iter,plot_loss
