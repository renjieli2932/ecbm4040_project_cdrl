# ECBM 4040 PROJECT
# Team: CDRL
# Renjie Li, Chenghao Diao

import tensorflow as tf
import numpy as np
from scipy.misc import imread
from scipy.misc import imresize
from scipy.misc import imsave
from scipy.io import loadmat

global_width = 400
global_height = 300

#############################################
#############   Image IO         ############
#############################################
class ImageIO(object):
    def __init__(self):
        # Resize all images to same size
        self.width = global_width
        self.height = global_height
        self.VGG_mean = np.array([[[[123, 117, 104]]]])
        #reduce mean: https://github.com/intel-analytics/BigDL/blob/master/docs/docs/APIGuide/Transformer.md

    def read(self, img_path):
        img = imread(img_path)
        resize_img = imresize(img, size=(self.height, self.width))
        exp_img = np.expand_dims(resize_img,axis=0) # expand dim to [1, 300,400,3] to meet the requirements of network
        exp_img = exp_img - self.VGG_mean
        return exp_img

    def write(self, img_name, img):
        img = img + self.VGG_mean
        red_img = img[0]
        uint_img = np.clip(red_img, 0, 255).astype('uint8') # post prococess the image to RGB format
        imsave(img_name,uint_img)
        print('Write to:',img_name)
        return uint_img

'''
Different from what we did in homework, we only need conv_layer and 
pooling_layer(average_pooling) in this network. Here I simply modify the
class structure provided in the previous assignments.
'''


#############################################
#############   Conv_layer       ############
#############################################
class conv_layer(object):
    def __init__(self, input_x, vgg_model, model_index):
        '''
        :param input_x: The input of the conv layer
        :param vgg_model: the pre-trained model we built
        :param model_index: Layer index , read the descriptions above def VGG19
        '''
        self.weight = tf.constant(vgg_model[model_index][0][0][0][0][0]) # weights
        self.bias = tf.constant(vgg_model[model_index][0][0][0][0][1]) # bias

        conv_out = tf.nn.conv2d(input_x, self.weight, strides=[1, 1, 1, 1], padding="SAME") 
        cell_out = tf.nn.relu(conv_out + self.bias) # convolution with ReLU activation

        self.cell_out = cell_out

    def output(self):
        return self.cell_out


#############################################
#############   Pooling Layer        ########
#############################################
class avg_pooling_layer(object):
    def __init__(self, input_x, k_size=2, padding="SAME"):
        """
        :param input_x: The input of the pooling layer.
        :param k_size: The kernel size you want to behave pooling action.
        :param padding: The padding setting. Read documents of tf.nn.max_pool for more information.
        """

        pooling_shape = [1, k_size, k_size, 1]
        cell_out = tf.nn.avg_pool(input_x, strides=pooling_shape, ksize=pooling_shape, padding=padding) 
        # use average pooling instead of max pooling
        self.cell_out = cell_out

    def output(self):
        return self.cell_out


#############################################
#############   VGG19                ########
#############################################

'''
The indices of the pretrained VGG 19 model are basically:
0 -> conv1_1
1 -> ReLu of conv1_1
2 -> conv1_2
3 -> ReLu of conv1_2
4 -> Pool of conv1
5 -> conv1_2
6 -> ReLu of conv1_2
......
'''


def VGG19(model_path):
    '''
    :param model_path: We use a pre-trained VGG19 model for initialization
    :return: VGG19 Network
    '''
    vgg_model = loadmat(model_path)
    vgg_model = vgg_model['layers'][0]  # we only need the weights info
    VGG19 = {}  # Since we need to extract info from different layers, use dict
    # to return all the layer info

    # Input Layer
    # Uses an assignment function - not a feed_dict - because in order for the style transfer to work, the input has become a tf variable, not a placeholder.
    # https://datascience.stackexchange.com/questions/23510/using-a-white-noise-image-to-minimise-the-loss-in-a-convolutional-neural-netwo?rq=1
    
    VGG19['input'] = tf.Variable(np.zeros((1, global_height, global_width, 3)), dtype=tf.float32)

    # Conv Later 1
    VGG19['conv1_1'] = conv_layer(VGG19['input'], vgg_model, 0)
    VGG19['conv1_2'] = conv_layer(VGG19['conv1_1'].output(), vgg_model, 2)
    VGG19['pool1'] = avg_pooling_layer(VGG19['conv1_2'].output())
    # Conv Layer 2
    VGG19['conv2_1'] = conv_layer(VGG19['pool1'].output(), vgg_model, 5)
    VGG19['conv2_2'] = conv_layer(VGG19['conv2_1'].output(), vgg_model, 7)
    VGG19['pool2'] = avg_pooling_layer(VGG19['conv2_2'].output())
    # Conv Layer 3
    VGG19['conv3_1'] = conv_layer(VGG19['pool2'].output(), vgg_model, 10)
    VGG19['conv3_2'] = conv_layer(VGG19['conv3_1'].output(), vgg_model, 12)
    VGG19['conv3_3'] = conv_layer(VGG19['conv3_2'].output(), vgg_model, 14)
    VGG19['conv3_4'] = conv_layer(VGG19['conv3_3'].output(), vgg_model, 16)
    VGG19['pool3'] = avg_pooling_layer(VGG19['conv3_4'].output())
    # Conv Layer 4
    VGG19['conv4_1'] = conv_layer(VGG19['pool3'].output(), vgg_model, 19)
    VGG19['conv4_2'] = conv_layer(VGG19['conv4_1'].output(), vgg_model, 21)
    VGG19['conv4_3'] = conv_layer(VGG19['conv4_2'].output(), vgg_model, 23)
    VGG19['conv4_4'] = conv_layer(VGG19['conv4_3'].output(), vgg_model, 25)
    VGG19['pool4'] = avg_pooling_layer(VGG19['conv4_4'].output())
    # Conv Layer 5
    VGG19['conv5_1'] = conv_layer(VGG19['pool4'].output(), vgg_model, 28)
    VGG19['conv5_2'] = conv_layer(VGG19['conv5_1'].output(), vgg_model, 30)
    VGG19['conv5_3'] = conv_layer(VGG19['conv5_2'].output(), vgg_model, 32)
    VGG19['conv5_4'] = conv_layer(VGG19['conv5_3'].output(), vgg_model, 34)
    VGG19['pool5'] = avg_pooling_layer(VGG19['conv5_4'].output())

    return VGG19


#############################################
#############   Loss Func            ########
#############################################
'''
Simply follow the definiton in the original paper.
Some of the methods we used to 'revise' the loss function 
have been commented. 
'''

def content_loss(p, x):
    '''
    :param p: original image, shape[1,rm1,rm2,n]
    :param x: image that generated, shape[1,rm1,rm2,n]
    :return: content loss
    '''
    M = p.shape[1] * p.shape[2]  # size of feature map
    N = p.shape[3]  # # of feature maps
    P = p.reshape(M,N)
    F = tf.reshape(x,(M,N))
    #print(p.shape[1],p.shape[2],p.shape[3])
    content_loss = 1.0 / 2 * tf.reduce_sum(tf.pow((P-F), 2))
    #content_loss = tf.reduce_mean(tf.square(P-F))
    #content_loss = tf.reduce_mean(tf.squared_difference(P,F))
    #content_loss = tf.losses.mean_squared_error(P.T,tf.transpose(F))
    return content_loss


def style_loss(a, x):
    '''
    :param a: original image, shape [1,rm1,rm2,n]
    :param x: image that generated, shape[1,rm1,rm2,n]
    :return: style loss
    '''

    M = a.shape[1] * a.shape[2]  # size of feature map
    N = a.shape[3]  # # of feature maps
    #print(a.shape[1],a.shape[2],a.shape[3])
    ori_img = a.reshape(M, N)
    A = ori_img.T.dot(ori_img)  # [N x N]
    
    gen_img = tf.reshape(x, (M, N))  # tensor
    G = tf.matmul(tf.transpose(gen_img), gen_img)  # [N x N]

    style_loss = 1.0 / (4 * N * N * M * M) * tf.reduce_sum(tf.pow((G - A), 2))
    #style_loss = tf.reduce_mean(tf.square(G-A))
    #style_loss = tf.reduce_mean(tf.squared_difference(G,A))
    #style_loss = tf.losses.mean_squared_error(G,A)
    return style_loss

